#include "proto.h"

#ifdef PROTO_STATIC
#  include <pb_encode.c>
#  include <pb_decode.c>
#  include "proto.pb.c"
#endif

static const char* _proto_error[] = {
  [-proto_no_error] = "No error",
  [-proto_end_of_packet] = "End of packet",
  [-proto_invalid_frame] = "Invalid frame",
  [-proto_overflow_buffer] = "Overflow buffer",
  [-proto_invalid_packet] = "Invalid packet",
  [-proto_sync_failed] = "Synchronization failed",
};

PROTO_EXTERN const char* proto_error_str(proto_val_t err){
  return _proto_error[err > 0 ? 0 : -err];
}

PROTO_EXTERN void proto_init(proto_t* self, proto_val_t cypher, const char* passwd){
  memset(&self->req, 0, sizeof(self->req));
  memset(&self->res, 0, sizeof(self->res));
  
  proto_begin(self);
  proto_end(self, 0);
}

PROTO_EXTERN proto_val_t proto_enc_req(proto_t* self){
  self->os = pb_ostream_from_buffer(self->buf, sizeof(self->buf));
  
  proto_begin(self);
  
  if(!pb_encode(&self->os, proto_req_fields, &self->req)){
    proto_empty(self);
    return proto_invalid_packet;
  }
  
  proto_end(self, self->os.bytes_written);
  
  return proto_no_error;
}

PROTO_EXTERN proto_val_t proto_dec_req(proto_t* self){
  self->is = pb_istream_from_buffer(self->buf, self->end - self->buf);
  
  proto_begin(self);
  proto_empty(self);
  
  if(!pb_decode(&self->is, proto_req_fields, &self->req)){
    return proto_invalid_packet;
  }
  
  return proto_no_error;
}

PROTO_EXTERN proto_val_t proto_enc_res(proto_t* self){
  self->os = pb_ostream_from_buffer(self->buf, sizeof(self->buf));
  
  proto_begin(self);
  
  if(!pb_encode(&self->os, proto_res_fields, &self->res)){
    proto_empty(self);
    return proto_invalid_packet;
  }
  
  proto_end(self, self->os.bytes_written);
  
  return proto_no_error;
}

PROTO_EXTERN proto_val_t proto_dec_res(proto_t* self){
  self->is = pb_istream_from_buffer(self->buf, self->end - self->buf);
  
  proto_begin(self);
  proto_empty(self);
  
  if(!pb_decode(&self->is, proto_res_fields, &self->res)){
    return proto_invalid_packet;
  }
  
  return proto_no_error;
}

PROTO_EXTERN proto_val_t proto_put_frame(proto_t* self, proto_raw_t* data, proto_len_t size){  
  if(!proto_is_empty(self) && proto_is_end(self)){ /* end of packet */
    return proto_end_of_packet;
  }
  
  pb_istream_t* stream = &self->is;
  proto_frame* frame = &self->fr;
  
  *stream = pb_istream_from_buffer(data, size);
  
  if(!pb_decode(stream, proto_frame_fields, frame)){
    return proto_invalid_frame;
  }
  
  if(frame->has_size){ /* initial frame */
    frame->has_size = 0;
    if(frame->size > sizeof(self->buf)){ /* check size */
      return proto_overflow_buffer;
    }
    /* init data pointer */
    proto_begin(self);
    /* set data edge */
    proto_end(self, frame->size);
  }
  
  if(proto_is_empty(self)){ /* no data */
    return proto_sync_failed;
  }
  
  if(proto_exceed_end(self, frame->data.size)){ /* check overflow */
    proto_empty(self); /* reset data edge */
    return proto_overflow_buffer;
  }
  memcpy(self->pos, frame->data.bytes, frame->data.size);
  proto_seek(self, frame->data.size);
  
  return frame->data.size;
}

/* frame overhead */
#define proto_fovh(size) ((size) < 1 ? 2 : (size) < (1 << 7) ? 4 : (size) < (1 << 14) ? 5 : 6)

PROTO_EXTERN proto_val_t proto_get_frame(proto_t* self, proto_raw_t* data, proto_len_t size){
  if(proto_is_empty(self)){ /* no data */
    return proto_end_of_packet;
  }
  
  pb_ostream_t* stream = &self->os;
  proto_frame* frame = &self->fr;
  
  proto_val_t fovh;
  
  if(proto_is_begin(self) && !frame->has_size){ /* initial frame */
    frame->has_size = 1;
    frame->size = proto_length(self);
    fovh = proto_fovh(frame->size);
  }else{
    frame->has_size = 0;
    fovh = proto_fovh(0);
  }
  
  *stream = pb_ostream_from_buffer(data, size);

  if(size > sizeof(frame->data.bytes)){
    size = sizeof(frame->data.bytes);
  }
  
  frame->data.size = size - fovh;
  
  if(proto_exceed_end(self, frame->data.size)){
    frame->data.size = proto_right(self);
  }
  
  memcpy(frame->data.bytes, self->pos, frame->data.size);
  proto_seek(self, frame->data.size);

  if(proto_is_end(self)){ /* end of packet */
    proto_begin(self);
    proto_empty(self);
  }
  
  if(!pb_encode(stream, proto_frame_fields, frame)){
    return proto_invalid_frame;
  }
  
  return stream->bytes_written;
}
