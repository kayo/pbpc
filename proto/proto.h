#include <pb_encode.h>
#include <pb_decode.h>
#include "proto.pb.h"

#ifdef PROTO_STATIC
#  define PROTO_EXTERN static
#else
#  define PROTO_EXTERN
#endif

#define proto_is(self, type) ((self).has_##type)

#define proto_max(a, b) ((a) > (b) ? (a) : (b))

typedef void proto_raw_t;
typedef uint8_t proto_len_t;
typedef int8_t proto_val_t;

/*
enum proto_state_e {
  proto_init = 0,
  proto_recv = 1,
  proto_proc = 2,
  proto_send = 3
};
*/
enum proto_error_e {
  proto_no_error = -0,
  proto_end_of_packet = -1,
  proto_invalid_frame = -2,
  proto_overflow_buffer = -3,
  proto_invalid_packet = -4,
  proto_sync_failed = -5,
};

PROTO_EXTERN const char* proto_error_str(proto_val_t err);

enum proto_cypher_e {
  proto_none = 0,
  /* block cypher algorithm */
  proto_aes128 = 1,
  proto_aes192 = 2,
  proto_aes256 = 3,
  /* block cypher mode */
  proto_ecb = 0,
  proto_cbc = 1 << 2,
  proto_cbf = 2 << 2,
  proto_obf = 3 << 2,
  proto_ctr = 4 << 2,
};

#define proto_cypher_is_alg(cyp, alg) (((cyp) & 0b00011) == proto_##alg)
#define proto_cypher_is_mod(cyp, mod) (((cyp) & 0b11100) == proto_##mod)

/*
#define proto_ctype_uint8_t proto_type_uint
#define proto_ctype_int8_t proto_type_sint
#define proto_ctype_uint16_t proto_type_uint
#define proto_ctype_int16_t proto_type_sint
#define proto_ctype_uint32_t proto_type_uint
#define proto_ctype_int32_t proto_type_sint
#define proto_ctype_int proto_type_sint
#define proto_ctype_char proto_type_sint
#define proto_ctype_float proto_type_real
#define proto_ctype_charp proto_type_cstr
*/
typedef struct {
  proto_val_t buf[proto_max(proto_req_size, proto_res_size) + 16];
  proto_val_t *pos; /* buffer reading position */
  proto_val_t *end; /* buffer writing edge */
  //proto_val_t len;
  
  proto_val_t state;
  
  proto_req req;
  proto_res res;
  
  /* static */
  pb_istream_t is;
  pb_ostream_t os;
  
  proto_frame fr;
  
  /* cypher */
  
} proto_t;

PROTO_EXTERN void proto_init(proto_t* self, proto_val_t cypher, const char* passwd);

PROTO_EXTERN proto_val_t proto_enc_req(proto_t* self);
PROTO_EXTERN proto_val_t proto_dec_req(proto_t* self);

PROTO_EXTERN proto_val_t proto_enc_res(proto_t* self);
PROTO_EXTERN proto_val_t proto_dec_res(proto_t* self);

PROTO_EXTERN proto_val_t proto_get_frame(proto_t* self, proto_raw_t* data, proto_len_t size);
PROTO_EXTERN proto_val_t proto_put_frame(proto_t* self, proto_raw_t* data, proto_len_t size);

#define proto_length(self) ((self)->end - (self)->buf)
#define proto_left(self) ((self)->pos - (self)->buf)
#define proto_right(self) ((self)->end - (self)->pos)

#define proto_is_empty(self) ((self)->end == (self)->buf)
#define proto_empty(self) ((self)->end = (self)->buf)

#define proto_is_begin(self) ((self)->pos == (self)->buf)
#define proto_begin(self) ((self)->pos = (self)->buf)
#define proto_seek(self, size) ((self)->pos += (size))

#define proto_is_end(self) ((self)->pos == (self)->end)
#define proto_end(self, size) ((self)->end = (self)->buf + (size))
#define proto_exceed_end(self, size) ((size) > proto_right(self))

#define proto_on(self) if(proto_dec_req(self) == proto_no_error)

#define proto_if(self, type)                                  \
  if(proto_is((self)->req, type))                             \
    for(proto_req_ ## type ## _ *req = &(self)->req.type;     \
        proto_is((self)->req, type);                          \
        proto_is((self)->req, type) = 0) /* reset req */      \
      for(proto_res_ ## type ## _ *res = &(self)->res.type;   \
          res;                                                \
          proto_is((self)->res, type) = 1, /* select res */   \
            proto_enc_res(self), /* encode res */             \
            proto_is((self)->res, type) = 0, /* reset res */  \
            res = NULL)

#define proto_do_req(self, type)                          \
  for(proto_req_ ## type ## _ *req = &(self)->req.type;   \
      req;                                                \
      proto_is((self)->req, type) = 1, /* select req */   \
        proto_enc_req(self), /* encode req */             \
        proto_is((self)->req, type) = 0, /* reset req */  \
        req = NULL)

#define proto_on_res(self, type)                          \
  if(proto_dec_res(self) == proto_no_error &&             \
     proto_is((self)->res, type))                         \
    for(proto_res_ ## type ## _ *res = &(self)->res.type; \
        proto_is((self)->res, type);                      \
        proto_is((self)->res, type) = 0) /* reset res */

#define proto_par_has_name(des) ((des)->has_name)
#define proto_par_set_name(des, _name_) { (des)->has_name = 1; strcpy((des)->name, (_name_)); }

#define proto_par_has_type(des) ((des)->has_type)
#define proto_par_set_type(des, _type_) { (des)->has_type = 1; (des)->type = (_type_); }

#define proto_par_has_attr(des) ((des)->has_attr)
#define proto_par_set_attr(des, _attr_) { (des)->has_attr = 1; (des)->attr = (_attr_); }

//#define proto_data_set(to, type, from) proto_data_set_(&(to), type, &(from))
//void proto_data_set_(proto_data* dat, proto_val_t type, proto_raw_t* data);

//#define proto_data_get(from, type, to) proto_par_get_val_(&(from), type, &(to))
//proto_val_t proto_data_get_(proto_data* data, proto_val_t type, proto_raw_t* val);

#define proto_data_clr(self) { \
    proto_is(self, flag) = 0;  \
    proto_is(self, uint) = 0;  \
    proto_is(self, sint) = 0;  \
    proto_is(self, real) = 0;  \
    proto_is(self, cstr) = 0;  \
    proto_is(self, uint) = 0;  \
  }

#define proto_data_set_atom(self, type, data) { \
    proto_data_clr(self);                       \
    proto_is(self, type) = 1;                   \
    (self).type = (data);                       \
  }

#define proto_data_set_area(self, type, data, size) { \
    proto_data_clr(self);                             \
    proto_is(self, type) = 1;                         \
    memcpy((self).type, (data), size);                \
  }

#define proto_data_get_atom(self, type, data) ({  \
        if(proto_is(self, type)){                 \
          (data) = (self).type;                   \
        }                                         \
        proto_is(self, type);                     \
      })

#define proto_data_get_area(self, type, data, size) ({  \
      if(proto_is(self, type)){                         \
        memcpy(data, (self).type, size);                \
      }                                                 \
      proto_is(self, type);                             \
    })

#define proto_data_set_flag(self, data) proto_data_set_atom(self, flag, data)
#define proto_data_set_uint(self, data) proto_data_set_atom(self, uint, data)
#define proto_data_set_sint(self, data) proto_data_set_atom(self, sint, data)
#define proto_data_set_real(self, data) proto_data_set_atom(self, real, data)

#define proto_data_set_cstr(self, data) proto_data_set_area(self, cstr, data, strlen(data) + 1)

#define proto_data_get_flag(self, data) proto_data_get_atom(self, flag, data)
#define proto_data_get_uint(self, data) proto_data_get_atom(self, uint, data)
#define proto_data_get_sint(self, data) proto_data_get_atom(self, sint, data)
#define proto_data_get_real(self, data) proto_data_get_atom(self, real, data)

#define proto_data_get_cstr(self, data) proto_data_get_atom(self, cstr, data)
#define proto_data_get_cstr_copy(self, data) proto_data_get_area(self, cstr, data, strlen(data) + 1)
