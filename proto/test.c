#include "proto.h"

#ifdef __linux__
#  include <stdio.h>
#  include <assert.h>
#  define info(fmt, arg...) fprintf(stdout, fmt "\n", ##arg)
#  define fail(fmt, arg...) fprintf(stderr, fmt "\n", ##arg)
#else
#  define assert(...)
#  define info(fmt, arg...)
#  define fail(fmt, arg...)
#endif

void proto_dev(proto_t *dev){
  proto_on(dev){
    proto_if(dev, par_cnt){
      res->cnt = 3;
      info("Res parameters count: %d", res->cnt);
    }
    proto_if(dev, par_des){
      info("Res parameter desc: %d", req->id);
      if(req->id < 3){
        switch(req->id){
        case 0:
          proto_par_set_name(res, "Par0");
          proto_par_set_type(res, proto_type_uint);
          proto_par_set_attr(res, proto_attr_ro);
          break;
        case 1:
          proto_par_set_name(res, "Parameter_1");
          proto_par_set_type(res, proto_type_real);
          proto_par_set_attr(res, proto_attr_rw);
          break;
        case 2:
          proto_par_set_name(res, "__Very_Very_Very_Long_Long_Long_Parameter_2__");
          proto_par_set_type(res, proto_type_cstr);
          res->has_attr = 0;
          break;
        }
      }
    }
    proto_if(dev, par_get){
      info("Res parameter val: %d", req->id);
      if(req->id < 3){
        res->has_val = 1;
        switch(req->id){
        case 0:
          proto_data_set_uint(res->val, 125);
          break;
        case 1:
          proto_data_set_real(res->val, -12.5);
          break;
        case 2:
          proto_data_set_cstr(res->val, "string value");
          break;
        }
      }
    }
  }
}

proto_t dev;
proto_t host;

#define proto_io_loop(from, to, frame_size) {                         \
    char sended = 0;                                                  \
    proto_val_t size;                                                 \
    proto_val_t data[frame_size];                                     \
    info("Send data size:%d " #from ">" #to " with frame size: %d",   \
         proto_length(from), frame_size);                             \
    for(; ; ){                                                        \
      size = proto_get_frame(from, data, frame_size);                 \
      if(size == proto_end_of_packet){                                \
        info(#from " send complete");                                 \
        sended = 1;                                                   \
      }else if(size < 0){                                             \
        fail(#from " get frame error: %s", proto_error_str(size));    \
        break;                                                        \
      }else{                                                          \
        info(#from " get frame size: %d", size);                      \
      }                                                               \
      size = proto_put_frame(to, data, size);                         \
      if(size == proto_end_of_packet){                                \
        if(sended){                                                   \
          info(#to " recv complete");                                 \
        }else{                                                        \
          fail("recv complete but send incomplete");                  \
        }                                                             \
        break;                                                        \
      }else if(sended){                                               \
        fail("send complete but recv incomplete");                    \
        break;                                                        \
      }else if(size < 0){                                             \
        fail(#to " put frame error: %s", proto_error_str(size));      \
        break;                                                        \
      }else{                                                          \
        info(#to " put data size: %d", size);                         \
      }                                                               \
    }                                                                 \
  }

void test_par_desc(proto_len_t par, proto_len_t frame, const char* name, proto_val_t type, proto_val_t attr){
  info("Req parameter %d desc", par);
  
  proto_do_req(&host, par_des){
    req->id = par;
  }
  
  proto_io_loop(&host, &dev, frame);
  
  proto_dev(&dev);
  
  proto_io_loop(&dev, &host, frame);
  
  proto_on_res(&host, par_des){
    info("Ret parameter desc: %s %d %d", res->name, res->type, res->attr);
    assert(strcmp(res->name, name) == 0);
    if(res->has_type){
      assert(res->type == type);
    }
    if(res->has_attr){
      assert(res->attr == attr);
    }
  }
}
  
void proto_test(proto_len_t frame){
  info("Req parameters count");
  
  proto_do_req(&host, par_cnt);
  
  proto_io_loop(&host, &dev, frame);
  proto_dev(&dev);
  proto_io_loop(&dev, &host, frame);
  
  proto_on_res(&host, par_cnt){
    info("Ret parameters count: %d", res->cnt);
    assert(res->cnt == 3);
  }
  
  test_par_desc(0, frame, "Par0", proto_type_uint, proto_attr_ro);
  test_par_desc(1, frame, "Parameter_1", proto_type_real, proto_attr_rw);
  test_par_desc(2, frame, "__Very_Very_Very_Long_Long_Long_Parameter_2__", proto_type_cstr, proto_attr_na);
  
  info("Req parameter value 0");
  
  proto_do_req(&host, par_get){
    req->id = 0;
  }
  
  proto_io_loop(&host, &dev, frame);
  proto_dev(&dev);
  proto_io_loop(&dev, &host, frame);
  
  proto_on_res(&host, par_get){
    info("Ret parameter 0 value: %d", res->val.uint);
    assert(res->val.uint == 125);
  }
  
  info("Req parameter value 1");
  
  proto_do_req(&host, par_get){
    req->id = 1;
  }
  
  proto_io_loop(&host, &dev, frame);
  proto_dev(&dev);
  proto_io_loop(&dev, &host, frame);
  
  proto_on_res(&host, par_get){
    info("Ret parameter 1 value: %f", res->val.real);
    assert(res->val.real == -12.5);
  }
  
  info("Req parameter value 2");
  
  proto_do_req(&host, par_get){
    req->id = 2;
  }
  
  proto_io_loop(&host, &dev, frame);
  proto_dev(&dev);
  proto_io_loop(&dev, &host, frame);
  
  proto_on_res(&host, par_get){
    info("Ret parameter 2 value: %s", res->val.cstr);
    assert(strcmp(res->val.cstr, "string value") == 0);
  }
}

int main(){
#ifdef __linux__
  uint8_t buffer[128];
  
#define test_enc(type, data...) {                             \
    pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer)); \
    type m = data;                                            \
    pb_encode(&stream, type##_fields, &m);                    \
    info(#type " =" #data " #%d\n", stream.bytes_written);    \
  }
  
  test_enc(proto_frame, {
      .data = {
        .size = 120
      }
    });
  
  test_enc(proto_frame, {
      .data = {
        .size = 120
      },
        .has_size = 1, .size = (1 << 7) - 1
    });

  test_enc(proto_frame, {
      .data = {
        .size = 120
      },
        .has_size = 1, .size = (1 << 14) - 1
    });

  test_enc(proto_frame, {
      .data = {
        .size = 120
      },
        .has_size = 1, .size = (1 << 21) - 1
    });

  test_enc(proto_frame, {
      .data = {
        .size = 120
      },
        .has_size = 1, .size = (1 << 28) - 1
    });
  
  test_enc(proto_req, { .has_par_cnt = 1 });
  test_enc(proto_req, { .has_par_des = 1 });
  test_enc(proto_req, { .has_par_get = 1 });
  
  test_enc(proto_req, { .has_par_set = 1 });
  test_enc(proto_req, { .has_par_set = 1, .par_set = { .val = { .has_uint = 1, .uint = 25 } } });
  test_enc(proto_req, { .has_par_set = 1, .par_set = { .val = { .has_sint = 1, .sint = -123 } } });
  test_enc(proto_req, { .has_par_set = 1, .par_set = { .val = { .has_sint = 1, .sint = 256 } } });
  test_enc(proto_req, { .has_par_set = 1, .par_set = { .val = { .has_real = 1, .real = 12.6e-2 } } });

  test_enc(proto_res, { .has_par_cnt = 1, .par_cnt = { .cnt = 1280 } });
  
  test_enc(proto_res, { .has_par_des = 1, .par_des = { .name = "parameter", } });
  test_enc(proto_res, { .has_par_des = 1, .par_des = { .name = "parameter", .has_type = 1 } });
  test_enc(proto_res, { .has_par_des = 1, .par_des = { .name = "parameter", .has_attr = 1 } });
  test_enc(proto_res, { .has_par_des = 1, .par_des = { .name = "parameter", .has_type = 1, .has_attr = 1 } });
  
  test_enc(proto_res, { .has_par_get = 1, .par_get = { .val = { .has_uint = 1, .uint = 123 } } });
  test_enc(proto_res, { .has_par_get = 1, .par_get = { .val = { .has_sint = 1, .sint = -123 } } });
  test_enc(proto_res, { .has_par_get = 1, .par_get = { .val = { .has_real = 1, .real = 12.6e-2 } } });
  
  test_enc(proto_res, { .has_par_set = 1, .par_set = { .ok = 0 } });
  test_enc(proto_res, { .has_par_set = 1, .par_set = { .ok = 1 } });
#endif
  
  proto_init(&dev, proto_none, NULL);
  proto_init(&host, proto_none, NULL);

  for(proto_len_t frame = 4; frame < 128; frame += 1){
    proto_test(frame);
  }
}
