NANOPB_DIR:=../nanopb

%.pb: %.proto
	protoc -I/usr/include -I$(NANOPB_DIR)/generator -I. -o$@ $<

%.pb.c %.pb.h: %.pb
	python $(NANOPB_DIR)/generator/nanopb_generator.py $<

#proto.opt?=fixed

define proto_def
  ifneq ($$(filter $(firstword $(subst :, ,$(1))),$$(proto.opt)),)
    proto.def+=PROTO_$(lastword $(subst :, ,$(1)))=1
  endif
endef

$(foreach def,fixed:FIXED int64:INT64 float:FLOAT string:STRING,$(eval $(call proto_def,$(def))))

proto.proto:=$(wildcard *.proto)
proto.src:=$(patsubst %.proto,%.pb.o,$(proto.proto)) $(NANOPB_DIR)/pb_encode.c $(NANOPB_DIR)/pb_decode.c
proto.obj:=$(patsubst %.c,%.o,$(proto.src))

obj+=$(proto.obj)

.PRECIOUS: %.pb %.pb.c %.pb.h

proto.clean:
	rm -f *.pb *.pb.c *.pb.h

clean: proto.clean

cc.dir+=$(NANOPB_DIR)
